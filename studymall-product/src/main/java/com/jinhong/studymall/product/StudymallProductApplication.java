package com.jinhong.studymall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 1、整合MyBatis-Plus
 *  1）导入依赖
 *  2）配置yml----配置数据源
 *           ----配置MyBatis-Plus
 */
@EnableFeignClients(basePackages = "com.jinhong.studymall.product.feign")
@MapperScan("com.jinhong.studymall.product.dao")
@EnableDiscoveryClient
@SpringBootApplication
public class StudymallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudymallProductApplication.class, args);
    }

}
