package com.jinhong.studymall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jinhong.common.utils.PageUtils;
import com.jinhong.studymall.product.entity.SkuSaleAttrValueEntity;

import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 16:10:48
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

