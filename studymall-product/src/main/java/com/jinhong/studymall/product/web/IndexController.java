package com.jinhong.studymall.product.web;

import com.jinhong.studymall.product.entity.CategoryEntity;
import com.jinhong.studymall.product.service.CategoryService;
import com.jinhong.studymall.product.vo.Catelog2Vo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 类名称：IndexController
 * --------------------------
 * <p>
 * 类描述：首页
 *
 * @author: wgf
 * @date: 2020/8/25 19:54
 */
@Controller
public class IndexController {

    @Autowired
    CategoryService categoryService;


    @GetMapping({"/","/index.html"})
    public String indexPage(Model model){
        //TODO 1、查询所有1级分类
        List<CategoryEntity> categoryEntities = categoryService.getLevel1Categorys();

        model.addAttribute("categorys",categoryEntities);

        return "index";
    }

    @ResponseBody
    @GetMapping("/index/catalog.json")
    public Map<String,List<Catelog2Vo>> getCatalogJson(){

        Map<String, List<Catelog2Vo>> catalogJson = categoryService.getCatalogJson();
        return catalogJson;
    }

}