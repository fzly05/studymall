package com.jinhong.studymall.product.feign;

import com.jinhong.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 类名称：WareFeignService
 * --------------------------
 * <p>
 * 类描述：库存服务远程调用
 *
 * @author: wgf
 * @date: 2020/8/25 7:50
 */
@FeignClient("studymall-ware")
public interface WareFeignService {

    /**
     * 方法1、R设计时可以加上泛型R<T>
     * 方法2、直接返回我们想要的结果
     * 方法3、自己封装解析结果
     * @param skuId
     * @return
     */
    @PostMapping("/ware/waresku/hasstock")
    R getSkusHasStock(@RequestBody List<Long> skuId);
}
