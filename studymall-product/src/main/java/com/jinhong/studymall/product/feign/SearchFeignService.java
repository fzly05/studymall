package com.jinhong.studymall.product.feign;

import com.jinhong.common.to.es.SkuEsModel;
import com.jinhong.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 类名称：SearchFeignService
 * --------------------------
 * <p>
 * 类描述：远程调用检索服务
 *
 * @author: wgf
 * @date: 2020/8/25 9:26
 */
@FeignClient("studymall-search")
public interface SearchFeignService {

    @PostMapping("/search/save/product")
    R productStatusUp(@RequestBody List<SkuEsModel> skuEsModels);
}
