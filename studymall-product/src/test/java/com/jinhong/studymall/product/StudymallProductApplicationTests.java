package com.jinhong.studymall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jinhong.studymall.product.entity.BrandEntity;
import com.jinhong.studymall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class StudymallProductApplicationTests {

    @Autowired
    BrandService brandService;



    @Test
    void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();
//        brandEntity.setDescript("中国制造");
//        brandEntity.setName("华为手机");
//        brandService.save(brandEntity);
//        brandEntity.setBrandId(2L);
//        brandEntity.setDescript("中嘎嘎嘎嘎嘎嘎嘎嘎嘎嘎国制作");
//
//
//        brandService.updateById(brandEntity);
//        System.out.println("更新-------------成功");


        List<BrandEntity> list = brandService.list(new QueryWrapper<>(brandEntity).eq("brand_id", 2L));
        list.forEach((item)->{
            System.out.println(item);
        });
    }

}
