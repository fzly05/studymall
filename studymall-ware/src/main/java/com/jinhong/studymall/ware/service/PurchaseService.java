package com.jinhong.studymall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jinhong.common.utils.PageUtils;
import com.jinhong.studymall.ware.entity.PurchaseEntity;
import com.jinhong.studymall.ware.vo.MergeVo;
import com.jinhong.studymall.ware.vo.PurchaseDoneVo;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 21:03:46
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnreceivePurchase(Map<String, Object> params);

    void mergePurchase(MergeVo mergeVo);

    void received(List<Long> ids);

    void done(PurchaseDoneVo doneVo);

}

