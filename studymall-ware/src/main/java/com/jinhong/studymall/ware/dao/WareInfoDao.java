package com.jinhong.studymall.ware.dao;

import com.jinhong.studymall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 21:03:46
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
