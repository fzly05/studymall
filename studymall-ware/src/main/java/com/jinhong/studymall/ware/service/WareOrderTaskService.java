package com.jinhong.studymall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jinhong.common.utils.PageUtils;
import com.jinhong.studymall.ware.entity.WareOrderTaskEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 21:03:46
 */
public interface WareOrderTaskService extends IService<WareOrderTaskEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

