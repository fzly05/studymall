package com.jinhong.studymall.ware;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@MapperScan("com.jinhong.studymall.ware.dao")
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class StudymallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudymallWareApplication.class, args);
    }

}
