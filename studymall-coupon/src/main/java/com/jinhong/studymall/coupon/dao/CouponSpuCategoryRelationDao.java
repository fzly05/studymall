package com.jinhong.studymall.coupon.dao;

import com.jinhong.studymall.coupon.entity.CouponSpuCategoryRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券分类关联
 * 
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 20:32:04
 */
@Mapper
public interface CouponSpuCategoryRelationDao extends BaseMapper<CouponSpuCategoryRelationEntity> {
	
}
