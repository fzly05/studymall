package com.jinhong.studymall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jinhong.common.utils.PageUtils;
import com.jinhong.studymall.coupon.entity.SeckillSkuNoticeEntity;

import java.util.Map;

/**
 * 秒杀商品通知订阅
 *
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 20:32:04
 */
public interface SeckillSkuNoticeService extends IService<SeckillSkuNoticeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

