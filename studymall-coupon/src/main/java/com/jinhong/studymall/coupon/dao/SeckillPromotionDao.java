package com.jinhong.studymall.coupon.dao;

import com.jinhong.studymall.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 20:32:04
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
