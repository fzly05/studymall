package com.jinhong.studymall.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class StudymallThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudymallThirdPartyApplication.class, args);
    }

}
