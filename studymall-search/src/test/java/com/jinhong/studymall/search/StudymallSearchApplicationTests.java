package com.jinhong.studymall.search;

import com.alibaba.fastjson.JSON;
import com.jinhong.studymall.search.config.StudymallElasticSearchConfig;
import lombok.Data;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class StudymallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @Test
    void searchData() throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("users");

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query();
//        searchSourceBuilder.from();
//        searchSourceBuilder.size();
//        searchSourceBuilder.aggregation();
        searchSourceBuilder.query(QueryBuilders.matchQuery("userName","Wang Guangfeng"));


        searchRequest.source(searchSourceBuilder);

        //SearchResponse searchResponse = client.search(searchRequest, StudymallElasticSearchConfig.COMMON_OPTIONS);

        //分析结果



    }


    /**
     * 测试存储数据到es
     */
    @Test
    void indexData() throws IOException {
        IndexRequest indexRequest = new IndexRequest("users");
        indexRequest.id("1");
        User user = new User();
        user.setUserName("Wang Guangfeng");
        user.setAge(40);
        user.setGender("男");

        String jsonString = JSON.toJSONString(user);

        indexRequest.source(jsonString, XContentType.JSON);

        IndexResponse index = client.index(indexRequest,StudymallElasticSearchConfig.COMMON_OPTIONS);
        System.out.println(index);

    }

    @Data
    class User{
        private String userName;
        private String gender;
        private Integer age;
    }

    @Test
    void contextLoads() {
        System.out.println(client);
    }

}
