package com.jinhong.studymall.search.service;

import com.jinhong.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * 类名称：ProductSaveService
 * --------------------------
 * <p>
 * 类描述：产品上架服务接口
 *
 * @author: wgf
 * @date: 2020/8/25 8:36
 */
public interface ProductSaveService {
    boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;

}
