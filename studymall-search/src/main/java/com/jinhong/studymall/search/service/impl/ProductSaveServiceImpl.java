package com.jinhong.studymall.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.jinhong.common.to.es.SkuEsModel;
import com.jinhong.studymall.search.config.StudymallElasticSearchConfig;
import com.jinhong.studymall.search.constant.EsConstant;
import com.jinhong.studymall.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：ProductSaveServiceImpl
 * --------------------------
 * <p>
 * 类描述：服务接口实现类
 *
 * @author: wgf
 * @date: 2020/8/25 8:43
 */
@Slf4j
@Service
public class ProductSaveServiceImpl implements ProductSaveService {

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Override
    public boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException {
        //保存到ES
        //1、给es建立索引 product,建立好映射关系

        //2、在es中保存数据  BulkRequest bulkRequest, RequestOptions options
        BulkRequest bulkRequest = new BulkRequest();

        for(SkuEsModel model : skuEsModels){
            //1、构造保存请求
            IndexRequest indexRequest = new IndexRequest(EsConstant.PRODUCT_INDEX);
            indexRequest.id(model.getSkuId().toString());
            String s = JSON.toJSONString(model);
            indexRequest.source(s, XContentType.JSON);

            bulkRequest.add(indexRequest);
        }

        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, StudymallElasticSearchConfig.COMMON_OPTIONS);
        //TODO 如果批量错误。。。。
        boolean b = bulk.hasFailures();
        List<String> collect = Arrays.stream(bulk.getItems()).map(item -> {
            return item.getId();
        }).collect(Collectors.toList());

        log.info("商品上架完成：{}，返回数据:{}",collect,bulk.toString());
        return b;

    }
}