package com.jinhong.studymall.search.controller;

import com.jinhong.common.exception.BizCodeEnume;
import com.jinhong.common.to.es.SkuEsModel;
import com.jinhong.common.utils.R;
import com.jinhong.studymall.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * 类名称：ElasticSaveController
 * --------------------------
 * <p>
 * 类描述：商品检索服务
 *
 * @author: wgf
 * @date: 2020/8/25 8:32
 */
@Slf4j
@RequestMapping("/search/save")
@RestController
public class ElasticSaveController {

    @Autowired
    ProductSaveService productSaveService;

    //商品上架
    @PostMapping("/product")
    public R productStatusUp(@RequestBody List<SkuEsModel> skuEsModels) throws IOException {

        boolean b = false;
        try {
            b = productSaveService.productStatusUp(skuEsModels);
        }catch (Exception e){
            log.error("ElasticSaveController商品上架错误：{}",e);
            return R.error(BizCodeEnume.PRODUCT_UP_EXCEPYION.getCode(),BizCodeEnume.PRODUCT_UP_EXCEPYION.getMsg());
        }
        if(!b) {
            return R.ok();
        }else {
            return R.error(BizCodeEnume.PRODUCT_UP_EXCEPYION.getCode(),BizCodeEnume.PRODUCT_UP_EXCEPYION.getMsg());
        }
    }


}