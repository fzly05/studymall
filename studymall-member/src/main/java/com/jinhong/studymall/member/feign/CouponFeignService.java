package com.jinhong.studymall.member.feign;

import com.jinhong.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 类名称：CouponFeignService
 * --------------------------
 * <p>
 * 类描述：远程调用接口
 *
 * @author: wgf
 * @date: 2020/8/12 22:30
 */
@FeignClient("studymall-coupon")
public interface CouponFeignService {
    @RequestMapping("/coupon/coupon/member/list")
    public R membercoupons();
}
