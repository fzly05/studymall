package com.jinhong.studymall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.jinhong.studymall.member.feign")
@EnableDiscoveryClient
@SpringBootApplication
public class StudymallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudymallMemberApplication.class, args);
    }

}
