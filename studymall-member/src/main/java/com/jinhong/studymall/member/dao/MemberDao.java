package com.jinhong.studymall.member.dao;

import com.jinhong.studymall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 20:44:39
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
