package com.jinhong.studymall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jinhong.common.utils.PageUtils;
import com.jinhong.studymall.member.entity.MemberReceiveAddressEntity;

import java.util.Map;

/**
 * 会员收货地址
 *
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 20:44:39
 */
public interface MemberReceiveAddressService extends IService<MemberReceiveAddressEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

