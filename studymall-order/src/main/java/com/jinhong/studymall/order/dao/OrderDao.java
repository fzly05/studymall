package com.jinhong.studymall.order.dao;

import com.jinhong.studymall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 21:01:14
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
