package com.jinhong.studymall.order.dao;

import com.jinhong.studymall.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 21:01:14
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {
	
}
