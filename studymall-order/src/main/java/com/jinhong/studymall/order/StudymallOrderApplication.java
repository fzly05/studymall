package com.jinhong.studymall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class StudymallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudymallOrderApplication.class, args);
    }

}
