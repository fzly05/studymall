package com.jinhong.studymall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jinhong.common.utils.PageUtils;
import com.jinhong.studymall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author wangguangfeng
 * @email 18853318081@163.com
 * @date 2020-08-12 21:01:14
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

