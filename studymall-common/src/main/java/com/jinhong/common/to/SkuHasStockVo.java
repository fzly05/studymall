package com.jinhong.common.to;

import lombok.Data;

/**
 * 类名称：SkuHasStockVo
 * --------------------------
 * <p>
 * 类描述：Sku库存Vo
 *
 * @author: wgf
 * @date: 2020/8/25 3:29
 */
@Data
public class SkuHasStockVo {

    private Long skuId;

    private Boolean hasStock;

}