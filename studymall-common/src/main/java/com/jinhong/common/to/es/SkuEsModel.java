package com.jinhong.common.to.es;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 类名称：SkuEsModel
 * --------------------------
 * <p>
 * 类描述：Sku全文检索数据模型
 *
 * @author: wgf
 * @date: 2020/8/25 2:04
 */
@Data
public class SkuEsModel {
    private Long skuId;
    private String skuTitle;
    private BigDecimal skuPrice;
    private String skuImg;

    private Long spuId;

    private Long saleCount;
    private Boolean hasStock;
    private Long hotScore;
    private Long brandId;
    private Long catalogId;
    private String brandName;
    private String brandImg;
    private String catalogName;
    private List<Attrs> attrs;

    @Data
    public static class Attrs{
        private Long attrId;
        private String attrName;
        private String attrValue;
    }

}